import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';

import { LocalRecipeService } from '../shared/local-recipe.service';
import { GetRecipes, GetRecipesEntity } from '../shared/recipe.action';
import { Recipe } from './recipe.model';

@Component({
  selector: 'app-recipe-book',
  templateUrl: './recipe-book.component.html',
  styleUrls: ['./recipe-book.component.scss'],
})
export class RecipeBookComponent implements OnInit {
  activeRecipe: Recipe = null;
  recipes = null;

  constructor(
    private store: Store,
    private localRecipeService: LocalRecipeService
  ) {}

  ngOnInit(): void {
    this.store.dispatch(new GetRecipes());
    this.store.dispatch(new GetRecipesEntity());
    this.localRecipeService.setInitDataToStorage();
  }
}
