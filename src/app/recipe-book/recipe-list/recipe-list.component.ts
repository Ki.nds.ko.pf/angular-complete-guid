import { Component, OnInit, ɵɵtrustConstantResourceUrl } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { RecipesEntity, allTags, Tags, TagsFilter } from 'src/app/shared/recipe-api.model';
import {
  GetRecipes,
  SetSelectedRecipesEntity,
} from 'src/app/shared/recipe.action';
import { RecipeState } from 'src/app/shared/recipe.state';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { map, startWith } from 'rxjs/operators';
import { MatChipInputEvent } from '@angular/material/chips';
import { GetRecipesEntityWithTags, GetRecipesEntityWithTagsNew } from './../../shared/recipe.action';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss'],
})
export class RecipeListComponent {
  /* @Select(RecipeState.getRecipeList) recipes: Observable<Recipe[]>; */
  @Select(RecipeState.getRecipesEntityList) recipes: Observable<
    RecipesEntity[]
  >;

  @Select(RecipeState.getRecipesEntityWithTags) recipesFilter: Observable<
    RecipesEntity[]
  >;


  allTags: Tags = allTags;
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  tagCtrl = new FormControl();
  filteredTags: Observable<string[]>;
  tags: string[] = [];

  tagsNew: TagsFilter = new TagsFilter(false, false, false, false, false, false, false, false, false);
  
  constructor(private store: Store) {
    this.filteredTags = this.tagCtrl.valueChanges.pipe(
      startWith(null),
      map((tag: string | null) =>
        tag ? this._filter(tag) : allTags.getAllTagsName().slice()
      )
    );
  }

  onSelected(payload: RecipesEntity) {
    
    this.store.dispatch(new GetRecipesEntityWithTagsNew(this.tagsNew));
    this.store.dispatch(new SetSelectedRecipesEntity(payload));
  }

  onAddRecipe() {
    /*  this.store.dispatch(new AddRecipe(new Recipe(2324, '', '', ''))); */
  }

  getSummaryPreview(recipe: RecipesEntity) {
    return recipe.summary.split('.', 1);
  }

  add(event: MatChipInputEvent): void {
    /*we will store the input and value in local variables.*/

    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      /*the input string will be pushed to the tag list.*/
      this.tags.push(value);
    }

    if (input) {
      /*after storing the input we will clear the input field.*/

      input.value = '';
    }
  }

  private addTag(value): void {
    /* console.log(value) */
    switch (value) {
      case 'vegetarian 🥕':
        this.tagsNew.vegetarian = true
        console.log("Erfolgreich")
        break;
      case 'vegan 🥦':
        this.tagsNew.vegan = true
        break;
      case 'gluten free ❌🌾':
        this.tagsNew.glutenFree = true
        break;
      case 'dairy free ❌🥛':
        this.tagsNew.dairyFree = true
        break;
      case 'cheap 💰':
        this.tagsNew.cheap = true
        break;
      case 'very healthy ❤️':
        this.tagsNew.veryHealthy = true
        break;
      case 'sustainable ♻️':
        this.tagsNew.sustainable = true
        break;
      case 'very popular 🌎':
        this.tagsNew.veryPopular = true
        break;
      case 'Low-FODMAP 🥗':
        this.tagsNew.lowFodmap = true
        break;

    }
  }

  remove(tag: string): void {
    console.log(tag);
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    console.log(event);
    this.addTag(event.option.viewValue);
    console.log(this.tagsNew)
    this.tags.push(event.option.viewValue);
    this.tagCtrl.setValue(null);



    //this.store.dispatch(new GetRecipesEntityWithTagsNew());

    /*   this.store.dispatch(new GetRecipesEntityWithTags(this.tags)); */
    /*  this.recipesList = test; */
    console.log('serve state with tags: ' + this.recipesFilter);
  }

  private mapperTagsFilter() {

  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return allTags
      .getAllTagsName()
      .filter((tag) => tag.toLowerCase().includes(filterValue));
  }



}