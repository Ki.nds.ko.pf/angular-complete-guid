import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { RecipesEntity } from 'src/app/shared/recipe-api.model';
import { RecipeState } from 'src/app/shared/recipe.state';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.scss'],
})
export class RecipeDetailComponent implements OnInit {
  @Select(RecipeState.getSelectedRecipesEntity)
  selectedRecipesEntity: Observable<RecipesEntity>;
  selected: RecipesEntity = null;
  constructor() {}

  ngOnInit(): void {
    this.selectedRecipesEntity.subscribe((recipe) => {
      if (recipe) {
        console.log(recipe);
        this.selected = recipe;
      }
    });
  }
}
