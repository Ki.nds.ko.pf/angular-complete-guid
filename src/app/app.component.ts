import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { LocalRecipeService } from './shared/local-recipe.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'ng4-complete-guide';

  constructor(
    @Inject(DOCUMENT) private _document: Document,
    private renderer: Renderer2,
    private localRecipeService: LocalRecipeService
  ) {}
  ngOnInit(): void {}

  switchMode(isDarkMode: boolean) {
    const hostClass = isDarkMode ? 'dark-theme' : 'light-theme';
    this.renderer.setAttribute(this._document.body, 'class', hostClass);
  }
}
