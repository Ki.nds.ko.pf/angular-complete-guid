import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Output, Component, EventEmitter } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent {
  links = ['Link 1', 'Link 2', 'Link 3'];

  // ! refactorn mit ngxs !!! --> darkmode in state
  @Output() readonly darkModeSwichted = new EventEmitter<boolean>();
  isDarkMode = false;

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver) {}

  onDarkModeSwichted({ checked }: MatSlideToggleChange) {
    this.darkModeSwichted.emit(checked);
    this.isDarkMode = !this.isDarkMode;
  }
}
