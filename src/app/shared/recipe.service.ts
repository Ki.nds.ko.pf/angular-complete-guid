import { Injectable } from '@angular/core';
/* import { HttpClient } from '@angular/common/http'; */
import { Recipe } from '../recipe-book/recipe.model';
import { v4 as uuidv4 } from 'uuid';
@Injectable({
  providedIn: 'root',
})
export class RecipeService {
  constructor(/* private http: HttpClient */) {}

  fetchRecipes() {
    /* return this.http.get<Recipe[]>(
      'https://jsonplaceholder.typicode.com/Recipes'
    ); */
    let recipes: Recipe[] = [
      new Recipe(
        uuidv4(),
        'Gambas',
        'This is a test Recipe',
        'https://multiplyillustration.com/img/9487ec7398d81017053c85ef20a9fe57.jpg'
      ),
      new Recipe(
        uuidv4(),
        'Hackbällchen',
        'Scharfe Hackbällchen in Tomatensauce',
        'https://www.kuechengoetter.de/uploads/media/1800x1200/05/80165-hackbaellchen-in-tomatensauce.jpg?v=2-3'
      ),
      new Recipe(
        uuidv4(),
        'Suppe',
        'Suppe mit Miso und Mie-Nudeln',
        'https://image.stern.de/8521672/t/y3/v5/w1440/r1.7778/-/alles-verwenden-s--123-dinn.jpg'
      ),
    ];

    return recipes;
  }

  deleteRecipe(id: number) {
    /*  return this.http.delete(
      'https://jsonplaceholder.typicode.com/Recipes/' + id
    ); */
  }

  addRecipe(payload: Recipe) {
    /*  return this.http.post<Recipe>(
      'https://jsonplaceholder.typicode.com/Recipes',
      payload
    ); */
    return payload;
  }

  updateRecipe(payload: Recipe, id: number) {
    /*  return this.http.put<Recipe>(
      'https://jsonplaceholder.typicode.com/Recipes/' + id,
      payload
    ); */
    return payload;
  }
}
