import { State, Action, StateContext, Selector } from '@ngxs/store';

import { tap } from 'rxjs/operators';
import { Recipe } from '../recipe-book/recipe.model';
import { RecipeService } from './recipe.service';
import {
  AddRecipe,
  DeleteRecipe,
  GetRecipes,
  SetSelectedRecipe,
  UpdateRecipe,
  AddRecipesEntity,
  DeleteRecipesEntity,
  GetRecipesEntity,
  SetSelectedRecipesEntity,
  UpdateRecipesEntity,
  GetRecipesEntityWithTags,
  GetRecipesEntityWithTagsNew,
} from './recipe.action';
import { Injectable } from '@angular/core';
import { RecipesEntity, Status, Tags, TagsFilter } from './recipe-api.model';
import { LocalRecipeService } from './local-recipe.service';

export class RecipeStateModel {
  Recipes: Recipe[];
  selectedRecipe: Recipe;
  RecipesEntity: RecipesEntity[];
  selectedRecipesEntity: RecipesEntity;
  filteredTags: RecipesEntity[];
  filteredTagsNew: RecipesEntity[];
}

@State<RecipeStateModel>({
  name: 'Recipes',
  defaults: {
    Recipes: [],
    selectedRecipe: null,
    RecipesEntity: [],
    selectedRecipesEntity: null,
    filteredTags: [],
    filteredTagsNew: null
  },
})
@Injectable()
export class RecipeState {
  constructor(
    private RecipeService: RecipeService,
    private localRecipeService: LocalRecipeService
  ) {}

  // ? Selector for normal Recipe Modal

  @Selector()
  static getRecipeList(state: RecipeStateModel) {
    return state.Recipes;
  }

  @Selector()
  static getSelectedRecipe(state: RecipeStateModel) {
    return state.selectedRecipe;
  }

  // ?  Selector for RecipesEntity Model from API

  @Selector()
  static getRecipesEntityList(state: RecipeStateModel) {
    return state.RecipesEntity;
  }

  @Selector()
  static getSelectedRecipesEntity(state: RecipeStateModel) {
    return state.selectedRecipesEntity;
  }

  // ? Actions for normal Recipe Modal

  @Action(GetRecipes)
  getRecipes({ getState, setState }: StateContext<RecipeStateModel>) {
    /*     return this.RecipeService.fetchRecipes().pipe(
      tap((result) => {
        const state = getState();
        setState({
          ...state,
          Recipes: result,
        });
      })
    ); */
    const state = getState();
    setState({
      ...state,
      Recipes: this.RecipeService.fetchRecipes(),
    });
  }

  @Action(AddRecipe)
  addRecipe(
    { getState, patchState }: StateContext<RecipeStateModel>,
    { payload }: AddRecipe
  ) {
    /*   return this.RecipeService.addRecipe(payload).pipe(
      tap((result) => {
        const state = getState();
        patchState({
          Recipes: [...state.Recipes, result],
        });
      })
    ); */
    const state = getState();
    patchState({
      Recipes: [...state.Recipes, this.RecipeService.addRecipe(payload)],
    });
  }

  @Action(UpdateRecipe)
  updateRecipe(
    { getState, setState }: StateContext<RecipeStateModel>,
    { payload, id }: UpdateRecipe
  ) {
    /*  return this.RecipeService.updateRecipe(payload, id).pipe(
      tap((result) => {
        const state = getState();
        const RecipeList = [...state.Recipes];
        const RecipeIndex = RecipeList.findIndex((item) => item.id === id);
        RecipeList[RecipeIndex] = result;
        setState({
          ...state,
          Recipes: RecipeList,
        });
      })
    ); */
    const state = getState();
    const RecipeList = [...state.Recipes];
    const RecipeIndex = RecipeList.findIndex((item) => item.id === id);
    RecipeList[RecipeIndex] = this.RecipeService.updateRecipe(payload, id);
    setState({
      ...state,
      Recipes: RecipeList,
    });
  }

  @Action(DeleteRecipe)
  deleteRecipe(
    { getState, setState }: StateContext<RecipeStateModel>,
    { id }: DeleteRecipe
  ) {
    /* return this.RecipeService.deleteRecipe(id).pipe(
      tap(() => {
        const state = getState();
        const filteredArray = state.Recipes.filter((item) => item.id !== id);
        setState({
          ...state,
          Recipes: filteredArray,
        });
      })
    ); */
    const state = getState();
    const filteredArray = state.Recipes.filter((item) => item.id !== id);
    setState({
      ...state,
      Recipes: filteredArray,
    });
  }

  @Action(SetSelectedRecipe)
  setSelectedRecipeId(
    { getState, setState }: StateContext<RecipeStateModel>,
    { payload }: SetSelectedRecipe
  ) {
    const state = getState();
    setState({
      ...state,
      selectedRecipe: payload,
    });
  }

  // ?  Actions for RecipesEntity Model from API

  @Action(GetRecipesEntity)
  getRecipesEntity({ getState, setState }: StateContext<RecipeStateModel>) {
    /*     return this.RecipeService.fetchRecipes().pipe(
      tap((result) => {
        const state = getState();
        setState({
          ...state,
          Recipes: result,
        });
      })
    ); */
    const state = getState();
    const fetchAll = this.localRecipeService.fetchAll();
    if (fetchAll[0] === Status.OK) {
      setState({
        ...state,
        RecipesEntity: fetchAll[1],
      });
    } else {
      throw new Error();
    }
  }

  @Action(AddRecipesEntity)
  addRecipesEntity(
    { getState, patchState }: StateContext<RecipeStateModel>,
    { payload }: AddRecipesEntity
  ) {
    /*   return this.RecipeService.addRecipe(payload).pipe(
      tap((result) => {
        const state = getState();
        patchState({
          Recipes: [...state.Recipes, result],
        });
      })
    ); */
    const state = getState();
    const post = this.localRecipeService.post(payload);
    if (post[0] === Status.OK) {
      patchState({
        RecipesEntity: [...state.RecipesEntity, post[1]],
      });
    } else {
      throw new Error();
    }
  }

  @Action(UpdateRecipesEntity)
  updateRecipesEntity(
    { getState, setState }: StateContext<RecipeStateModel>,
    { payload, id }: UpdateRecipesEntity
  ) {
    /*  return this.RecipeService.updateRecipe(payload, id).pipe(
      tap((result) => {
        const state = getState();
        const RecipeList = [...state.Recipes];
        const RecipeIndex = RecipeList.findIndex((item) => item.id === id);
        RecipeList[RecipeIndex] = result;
        setState({
          ...state,
          Recipes: RecipeList,
        });
      })
    ); */
    const state = getState();
    const RecipeList = [...state.RecipesEntity];
    const RecipeIndex = RecipeList.findIndex((item) => item.id === id);
    const push = this.localRecipeService.push(payload, id);
    if (push[0] === Status.OK) {
      setState({
        ...state,
        RecipesEntity: RecipeList,
      });
    } else {
      throw new Error();
    }
  }

  @Action(DeleteRecipesEntity)
  deleteRecipesEntity(
    { getState, setState }: StateContext<RecipeStateModel>,
    { id }: DeleteRecipesEntity
  ) {
    /* return this.RecipeService.deleteRecipe(id).pipe(
      tap(() => {
        const state = getState();
        const filteredArray = state.Recipes.filter((item) => item.id !== id);
        setState({
          ...state,
          Recipes: filteredArray,
        });
      })
    ); */

    // ! ist das richtig ?
    const state = getState();
    const filteredArray = state.RecipesEntity.filter((item) => item.id !== id);
    const deleteRecipesEntity = this.localRecipeService.delete(id);
    if (deleteRecipesEntity[0] === Status.OK) {
      setState({
        ...state,
        RecipesEntity: filteredArray,
      });
    } else {
      throw new Error();
    }
  }

  @Action(SetSelectedRecipesEntity)
  setSelectedRecipesEntity(
    { getState, setState }: StateContext<RecipeStateModel>,
    { payload }: SetSelectedRecipesEntity
  ) {
    const state = getState();
    setState({
      ...state,
      selectedRecipesEntity: payload,
    });
  }

  @Action(GetRecipesEntityWithTags)
  GetRecipesEntityWithTags(
    { getState, setState }: StateContext<RecipeStateModel>,
    { tags }: GetRecipesEntityWithTags
  ) {
    /*     return this.RecipeService.fetchRecipes().pipe(
      tap((result) => {
        const state = getState();
        setState({
          ...state,
          Recipes: result,
        });
      })
    ); */
    const state = getState();
    const filterTags = this.localRecipeService.filterTags(tags);
    if (filterTags[0] === Status.OK) {
      setState({
        ...state,
        filteredTags: filterTags[1],
      });
    } else {
      throw new Error();
    }
  }

  @Action(GetRecipesEntityWithTagsNew)
  GetRecipesEntityWithTagsNew(
    { getState, setState }: StateContext<RecipeStateModel>,
    { tags }: GetRecipesEntityWithTagsNew
  ) {
    /*     return this.RecipeService.fetchRecipes().pipe(
      tap((result) => {
        const state = getState();
        setState({
          ...state,
          Recipes: result,
        });
      })
    ); */
    const state = getState();
    const filterTags = this.localRecipeService.filterTagsNew(tags);
    if (filterTags[0] === Status.OK) {
      console.log("filterRecipes" + filterTags[1].length);
      setState({
        ...state,
        filteredTagsNew: filterTags[1],
      });
    } else {
      throw new Error();
    }
  }

  @Selector()
  static getRecipesEntityWithTags(state: RecipeStateModel) {
    return state.filteredTags;
  }
}
