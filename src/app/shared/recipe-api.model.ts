export enum Status {
  OK,
  ERROR,
}
export interface RecipeApi {
  recipes?: RecipesEntity[] | null;
}
export interface RecipesEntity {
  vegetarian: boolean;
  vegan: boolean;
  glutenFree: boolean;
  dairyFree: boolean;
  veryHealthy: boolean;
  cheap: boolean;
  veryPopular: boolean;
  sustainable: boolean;
  weightWatcherSmartPoints: number;
  gaps: string;
  lowFodmap: boolean;
  aggregateLikes: number;
  spoonacularScore: number;
  healthScore: number;
  creditsText: string;
  license: string;
  sourceName: string;
  pricePerServing: number;
  extendedIngredients?: ExtendedIngredientsEntity[] | null;
  id: number;
  title: string;
  readyInMinutes: number;
  servings: number;
  sourceUrl: string;
  image: string;
  imageType: string;
  summary: string;
  cuisines?: null[] | null;
  dishTypes?: string[] | null;
  diets?: string[] | null;
  occasions?: null[] | null;
  instructions: string;
  analyzedInstructions?: AnalyzedInstructionsEntity[] | null;
  originalId?: null;
  spoonacularSourceUrl: string;
}
export interface ExtendedIngredientsEntity {
  id: number;
  aisle: string;
  image: string;
  consistency: string;
  name: string;
  nameClean: string;
  original: string;
  originalString: string;
  originalName: string;
  amount: number;
  unit: string;
  meta?: (string | null)[] | null;
  metaInformation?: (string | null)[] | null;
  measures: Measures;
}
export interface Measures {
  us: UsOrMetric;
  metric: UsOrMetric;
}
export interface UsOrMetric {
  amount: number;
  unitShort: string;
  unitLong: string;
}
export interface AnalyzedInstructionsEntity {
  name: string;
  steps?: StepsEntity[] | null;
}
export interface StepsEntity {
  number: number;
  step: string;
  ingredients?: IngredientsEntityOrEquipmentEntity[] | null;
  equipment?: (IngredientsEntityOrEquipmentEntity1 | null)[] | null;
  length?: Length | null;
}
export interface IngredientsEntityOrEquipmentEntity {
  id: number;
  name: string;
  localizedName: string;
  image: string;
}
export interface IngredientsEntityOrEquipmentEntity1 {
  id: number;
  name: string;
  localizedName: string;
  image: string;
}
export interface Length {
  number: number;
  unit: string;
}


 enum TagsEnum {
  vegetarian = "vegetarian",
}
/* 
export const allTagsWithIcon: string[] = [
  'vegetarian 🥕',
  'vegan 🥦',
  'gluten free 🌾',
  'dairy free ❌🥛',
  'very healthy ❤️',
  'cheap 💰',
  'very popular 🌎',
  'sustainable ♻️',
  'Low-FODMAP 🥗',
];
 */
export class Tags {
  _tags: Tag[];

  constructor(tags: Tag[]) {
    this._tags = tags;
  }

  getAllTagsName(): string[] {
    return this._tags.map((tag) => {
      return tag.getFullName();
    });
  }

  getVegie() {
    return this._tags.find((e) => e._icon === '🥕');
  }
  getVegan() {
    return this._tags.find((e) => e._icon === '🥦');
  }
  getGluten() {
    return this._tags.find((e) => e._icon === '❌🌾');
  }
  getDairy() {
    return this._tags.find((e) => e._icon === '❌🥛');
  }
  getHealthy() {
    return this._tags.find((e) => e._icon === '❤️');
  }
  getCheap() {
    return this._tags.find((e) => e._icon === '💰');
  }
  getPopular() {
    return this._tags.find((e) => e._icon === '🌎');
  }
  getSustain() {
    return this._tags.find((e) => e._icon === '♻️');
  }
  getLow() {
    return this._tags.find((e) => e._icon === '🥗');
  }
}

export class Tag {
  _key: number;
  _name: string;
  _propertyName: string;
  _icon: string;

  constructor(key: number, name: string, propertyName: string, icon: string) {
    this._key = key;
    this._name = name;
    this._propertyName = propertyName;
    this._icon = icon;
  }
  getFullName() {
    return this._name + ' ' + this._icon;
  }
}

export const allTags: Tags = new Tags([
  new Tag(1, 'vegetarian', 'vegetarian', '🥕'),
  new Tag(2, 'vegan', 'vegan', '🥦'),
  new Tag(3, 'gluten free', 'glutenFree', '❌🌾'),
  new Tag(4, 'dairy free', 'dairyFree', '❌🥛'),
  new Tag(5, 'cheap', 'cheap', '💰'),
  new Tag(6, 'very healthy', 'veryHealthy', '❤️'),
  new Tag(7, 'sustainable', 'sustainable', '♻️'),
  new Tag(8, 'very popular', 'veryPopular', '🌎'),
  new Tag(9, 'Low-FODMAP', 'LowFodmap', '🥗'),
]);

export const allTagsTest: Tag[] = [
  new Tag(1, 'vegetarian', 'vegetarian', '🥕'),
  new Tag(2, 'vegetarian', 'vegetarian', '🥕'),
  new Tag(3, 'vegetarian', 'vegetarian', '🥕'),
  new Tag(4, 'vegetarian', 'vegetarian', '🥕'),
  new Tag(5, 'vegetarian', 'vegetarian', '🥕'),
];


export class TagsFilter {
  constructor(  vegetarian: boolean,
    vegan: boolean,
    glutenFree: boolean,
    dairyFree: boolean,
    cheap: boolean,
    veryHealthy: boolean,
    sustainable: boolean,
    veryPopular: boolean,
    lowFodmap: boolean ) {
      this.vegetarian = vegetarian;
      this.vegan = vegan;
      this.glutenFree = glutenFree;
      this.dairyFree = dairyFree;
      this.cheap = cheap;
      this.veryHealthy = veryHealthy;
      this.sustainable = sustainable;
      this.veryPopular =veryPopular;
      this.lowFodmap = lowFodmap;
    }
  vegetarian: boolean;
  vegan: boolean;
  glutenFree: boolean;
  dairyFree: boolean;
  cheap: boolean;
  veryHealthy: boolean;
  sustainable: boolean;
  veryPopular: boolean;
  lowFodmap: boolean;
}
