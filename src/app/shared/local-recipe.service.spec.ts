import { TestBed } from '@angular/core/testing';
import { RecipeApi, RecipesEntity, Status } from './recipe-api.model';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';

import { LocalRecipeService } from './local-recipe.service';

describe('LocalRecipeService', () => {
  let service: LocalRecipeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [LocalRecipeService],
    });
    service = TestBed.inject(LocalRecipeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('get gives correct results', () => {
    let testGet: [Status, RecipesEntity] = service.get(993462);
    console.log(testGet);
    expect(testGet[0]).toEqual(Status.OK);
  });
});
