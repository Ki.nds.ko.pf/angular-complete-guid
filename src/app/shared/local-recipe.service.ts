import { Inject, Injectable } from '@angular/core';
import { Recipe } from '../recipe-book/recipe.model';
import { HttpClient } from '@angular/common/http';
import { allTags, RecipeApi, RecipesEntity, Status, TagsFilter } from './recipe-api.model';
import { Observable } from 'rxjs';
import { saveAs } from 'file-saver';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import * as data from '../data.json';

// key that is used to access the data in local storageconst
const STORAGE_KEY = 'local_recipelist';

@Injectable({
  providedIn: 'root',
})
export class LocalRecipeService {
  anotherRecipelist = [];
  constructor(
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private http: HttpClient
  ) {}

  public storeElementOnLocalStorage(recipe: any): void {
    // get array of tasks from local storage
    const currentRecipeList = this.storage.get(STORAGE_KEY) || [];
    // push new task to array
    currentRecipeList.push(recipe);
    // insert updated array to local storage
    this.storage.set(STORAGE_KEY, currentRecipeList);
    console.log(this.storage.get(STORAGE_KEY) || 'LocaL storage is empty');
  }

  public storeListOnLocalStorage(recipe: any): void {
    // get array of tasks from local storage
    const currentRecipeList = this.storage.get(STORAGE_KEY) || [];
    console.log(currentRecipeList);
    // push new task to array
    console.log(recipe);
    currentRecipeList.push(...recipe);
    console.log(currentRecipeList);
    // insert updated array to local storage
    this.storage.set(STORAGE_KEY, currentRecipeList);
    console.log(this.storage.get(STORAGE_KEY) || 'LocaL storage is empty');
  }

  public storeResetRecipeList(): void {
    this.storage.remove(STORAGE_KEY);
  }

  get(id: number): [Status, RecipesEntity] {
    const currentRecipeList = this.storage.get(STORAGE_KEY) || [];
    const newList = currentRecipeList as Array<RecipesEntity>;
    const result: RecipesEntity | undefined = newList.find(
      (recipe) => recipe.id === id
    );
    if (result) {
      return [Status.OK, result];
    }
    return [Status.ERROR, null];
  }
  fetchAll(): [Status, RecipesEntity[]] {
    const currentRecipeList = this.storage.get(STORAGE_KEY) || [];
    const newList = currentRecipeList as Array<RecipesEntity>;
    return [Status.OK, newList];
  }
  push(recipe: RecipesEntity, id: number): [Status, RecipesEntity] {
    const currentRecipeList = this.storage.get(STORAGE_KEY) || [];
    const newList = currentRecipeList as Array<RecipesEntity>;
    const newNewList = newList.map((recipeE) => {
      if (recipeE.id === id) {
        recipeE = recipe;
      }
      return recipeE;
    });
    this.storage.set(STORAGE_KEY, newNewList);
    return [Status.OK, recipe];
  }
  delete(id: number): Status {
    const currentRecipeList = this.storage.get(STORAGE_KEY) || [];
    const newList = currentRecipeList as Array<RecipesEntity>;
    const newNewList = newList.map((recipe) => {
      if (recipe.id !== id) {
        return recipe;
      }
    });
    this.storage.set(STORAGE_KEY, newNewList);
    return Status.OK;
  }
  public post(recipe: RecipesEntity): Status {
    const currentRecipeList = this.storage.get(STORAGE_KEY) || [];
    currentRecipeList.push(recipe);
    this.storage.set(STORAGE_KEY, currentRecipeList);
    return Status.OK;
  }

  public filterTags(tags: string[]): [Status, RecipesEntity[]] {
    const currentRecipeList = this.storage.get(STORAGE_KEY) || [];
    const newList = currentRecipeList as Array<RecipesEntity>;
    const filteredTags: RecipesEntity[] = [];
    newList.map((recipe) => {
      tags.forEach((tag) => {
        const newTag = this.getFilteredTagRecipesEntity(tag, recipe);
        if (newTag != undefined || newTag != null) {
          filteredTags.push(newTag);
        }
      });
    });
    console.log(filteredTags);
    return [Status.OK, filteredTags];
  }

  public filterTagsNew(tags: TagsFilter): [Status, RecipesEntity[]] {
    const currentRecipeList = this.storage.get(STORAGE_KEY) || [];
    const newList = currentRecipeList as Array<RecipesEntity>;
    const filteredTags: RecipesEntity[] = [];
    const result = newList.filter((recipe) => {
      if(false === false) { console.log("dfsfd")}
      if (tags.vegetarian === recipe.vegetarian &&
        tags.vegan === recipe.vegan &&
        tags.glutenFree === recipe.glutenFree &&
        tags.dairyFree === recipe.dairyFree &&
        tags.cheap === recipe.cheap &&
        tags.veryHealthy === recipe.veryHealthy &&
        tags.sustainable === recipe.sustainable &&
        tags.veryPopular === recipe.veryPopular &&
        tags.lowFodmap === recipe.lowFodmap) {
          filteredTags.push(recipe)
        }

    })
    console.log("hallo test"  + filteredTags.length);
    return [Status.OK, newList];
  }

  private getFilteredTagRecipesEntity(tag: string, recipe: RecipesEntity) {
    switch (tag) {
      case allTags.getVegie().getFullName():
        if (recipe.vegetarian) {
          return recipe;
        }
        break;
      case allTags.getVegan().getFullName():
        if (recipe.vegan) {
          console.log(recipe.vegan);
          return recipe;
        }
        break;
      case allTags.getGluten().getFullName():
        if (recipe.glutenFree) {
          return recipe;
        }
        break;
      case allTags.getDairy().getFullName():
        if (recipe.dairyFree) {
          return recipe;
        }
        break;
      case allTags.getHealthy().getFullName():
        if (recipe.veryHealthy) {
          return recipe;
        }
        break;
      case allTags.getCheap().getFullName():
        if (recipe.cheap) {
          return recipe;
        }
        break;
      case allTags.getPopular().getFullName():
        if (recipe.veryPopular) {
          return recipe;
        }
        break;
      case allTags.getSustain().getFullName():
        if (recipe.sustainable) {
          return recipe;
        }
        break;
      case allTags.getLow().getFullName():
        if (recipe.lowFodmap) {
          return recipe;
        }
        break;
      default:
        break;
    }
  }

  // tslint:disable-next-line: typedef
  public setInitDataToStorage() {
    /* this.getApiDataPlaceholder(); */
  }

  // tslint:disable-next-line: typedef
  public getApiDataPlaceholder() {
    this.http
      .get(
        'https://api.spoonacular.com/recipes/random?number=150&limitLicense=true&apiKey=87aaa14ce2414d39b13a28cd847c1bd7'
      )
      .subscribe((users) => {
        const usersAPI = users as RecipeApi;
        console.log(usersAPI.recipes);
        this.storeListOnLocalStorage(usersAPI.recipes);
      });
  }

  // tslint:disable-next-line: typedef
  public getJSONData() {
    const recipesEntityList: any[] = [];
    const test = data;

    console.log(test.recipes);
    this.storeListOnLocalStorage(test.recipes);
  }
}
