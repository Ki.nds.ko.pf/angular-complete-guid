import { Recipe } from '../recipe-book/recipe.model';
import { RecipesEntity, TagsFilter } from './recipe-api.model';

export class AddRecipe {
  static readonly type = '[Recipe] Add';

  constructor(public payload: Recipe) {}
}

export class GetRecipes {
  static readonly type = '[Recipe] Get';
}

export class UpdateRecipe {
  static readonly type = '[Recipe] Update';

  constructor(public payload: Recipe, public id: number) {}
}

export class DeleteRecipe {
  static readonly type = '[Recipe] Delete';

  constructor(public id: number) {}
}

export class SetSelectedRecipe {
  static readonly type = '[Recipe] Set';

  constructor(public payload: Recipe) {}
}

export class AddRecipesEntity {
  static readonly type = '[RecipesEntity] Add';

  constructor(public payload: RecipesEntity) {}
}

export class GetRecipesEntity {
  static readonly type = '[RecipesEntity] Get';
}

export class UpdateRecipesEntity {
  static readonly type = '[RecipesEntity] Update';

  constructor(public payload: RecipesEntity, public id: number) {}
}

export class DeleteRecipesEntity {
  static readonly type = '[RecipesEntity] Delete';

  constructor(public id: number) {}
}

export class SetSelectedRecipesEntity {
  static readonly type = '[RecipesEntity] Set';

  constructor(public payload: RecipesEntity) {}
}

export class GetRecipesEntityWithTags {
  static readonly type = '[RecipesEntity] Filter';

  constructor(public tags: string[]) {}
}




export class GetRecipesEntityWithTagsNew {
  static readonly type = '[RecipesEntity] Filter New';

  constructor(public tags: TagsFilter) {}
}